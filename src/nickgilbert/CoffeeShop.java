package nickgilbert;
/**
 * GUI program to run a coffee/bagel shoppe
 * @author Nick Gilbert
 */
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class CoffeeShop extends JPanel {
	public static JFrame mainFrame;
	private static JPanel menu, tr, co, cl, bl, pl, r, cb;
	public static int selectedList; //Coffee = 0, Bagel = 1, Pastry = 2
	
	public static int getSelectedList() {
		return selectedList;
	}

	public static void setSelectedList(int selectedList) {
		CoffeeShop.selectedList = selectedList;
	}

	public CoffeeShop()
	{
		//Setting up mainframe configurations
		mainFrame = new JFrame();
		mainFrame.setTitle("Gilbert Coffee Shop!");
     	mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     	mainFrame.setLayout(new BorderLayout());
     	selectedList = 1;
		
     	//Creating components of GUI window
     	menu = new TopMenu();
     	tr = new TitleRegister();
     	co = new CategoryOptions();
     	cl = new CoffeeList();
     	bl = new BagelList();
     	pl = new PastryList();
     	r = new Receipt();
     	cb = new ControlButtons();
     	
		//Piecing together GUI window
     	mainFrame.add(menu, BorderLayout.NORTH);
     	mainFrame.setJMenuBar(TopMenu.getMenuBar());
		mainFrame.add(tr, BorderLayout.NORTH);
		mainFrame.add(co, BorderLayout.WEST);
		mainFrame.add(bl, BorderLayout.CENTER);
		mainFrame.add(r, BorderLayout.EAST);
		mainFrame.add(cb, BorderLayout.SOUTH);
		mainFrame.pack();
		mainFrame.setVisible(true);
	}
	
	//Method that controls whether Coffee, bagel, or pastry options are shown
	public static void changeOptions(int toWhat) {
		if(toWhat == 0) {
			mainFrame.remove(bl);
			mainFrame.remove(pl);
			mainFrame.add(cl, BorderLayout.CENTER);
			selectedList = toWhat;
		}
		else if(toWhat == 1) {
			mainFrame.remove(cl);
			mainFrame.remove(pl);
			mainFrame.add(bl, BorderLayout.CENTER);
			selectedList = toWhat;
		}
		else if(toWhat == 2) {
			mainFrame.remove(cl);
			mainFrame.remove(bl);
			mainFrame.add(pl, BorderLayout.CENTER);
			selectedList = toWhat;
		}
		else
			JOptionPane.showMessageDialog(null, "Error, please select valid panel");
		
		//Resetting window
		mainFrame.revalidate();
		mainFrame.repaint();
	}
	
	public static void changeColorScheme(Color newColor) {
		tr.setBackground(newColor);
		co.setBackground(newColor);
		cl.setBackground(newColor);
		bl.setBackground(newColor);
		pl.setBackground(newColor);
		r.setBackground(newColor);
		cb.setBackground(newColor);
		
	}
	
	public static void main(String[] args) {
		new CoffeeShop();
	}
}
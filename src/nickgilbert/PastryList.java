package nickgilbert;
/**
 * Class to add list of pastries to register
 * @author Nick Gilbert
 */

import javax.swing.*;

import java.awt.*;
import java.awt.Event.*;

public class PastryList extends JPanel {
	
	public static JList pastryOptions;
	private String[] pastries = {"Apricot Danish", "Prune Danish", "Crossant", "Chocolate-Filled Crossant"};
	
	public PastryList() {
		setBorder(BorderFactory.createTitledBorder("Pastries"));
		pastryOptions = new JList(pastries);
		add(pastryOptions);
	}
	
	public static Order buildOrder() { //Method that builds an Order object based on input into cash register
		String orderDescription = "";
		double orderCost = 0.00;
		
		//Pastry options
		String selectedPastry = pastryOptions.getSelectedValue().toString();
		if(selectedPastry.equalsIgnoreCase("Apricot Danish")) {
			orderDescription += selectedPastry;
			orderCost += 1.25;
		}
		else if(selectedPastry.equalsIgnoreCase("Prune Danish")) {
			orderDescription += selectedPastry;
			orderCost += 1.25;
		}
		else if(selectedPastry.equalsIgnoreCase("Crossant")) {
			orderDescription += selectedPastry;
			orderCost += 1.50;
		}
		else if(selectedPastry.equalsIgnoreCase("Chocolate-Filled Crossant")) {
			orderDescription += selectedPastry;
			orderCost += 2.00;
		}
		else
			JOptionPane.showMessageDialog(null, "Error, please select valid option");
		
		return (new Order(orderDescription, orderCost));
	}
}

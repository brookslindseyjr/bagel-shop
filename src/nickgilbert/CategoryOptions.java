package nickgilbert;
/**
 * Creates Radio Button options in West Side of main windows
 * @author Nick Gilbert
 */
import javax.swing.*;

import java.awt.*;
import java.awt.Event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CategoryOptions extends JPanel {
	private JRadioButton coffeeButton, bagelButton, pastryButton;
	private ButtonGroup mainOptions;
	
	public CategoryOptions() {
		//Setting up panel
		setLayout(new GridLayout(3, 1));
     	setBorder(BorderFactory.createTitledBorder("Products"));
     	coffeeButton = new JRadioButton("Coffee");
		bagelButton = new JRadioButton("Bagels", true);
		pastryButton = new JRadioButton("Pastries");
		
		coffeeButton.addActionListener(new RadioListener());
		bagelButton.addActionListener(new RadioListener());
		pastryButton.addActionListener(new RadioListener());
		
		mainOptions = new ButtonGroup();
		
		mainOptions.add(bagelButton); add(bagelButton);
		mainOptions.add(pastryButton); add(pastryButton);
		mainOptions.add(coffeeButton); add(coffeeButton);
	}
	
	//Action listener that calls the CoffeeShop class' changeOption method to change middle panel
	private class RadioListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == coffeeButton) {
				CoffeeShop.changeOptions(0);
			}
			else if(e.getSource() == bagelButton) {
				CoffeeShop.changeOptions(1);
			}
			else if(e.getSource() == pastryButton) {
				CoffeeShop.changeOptions(2);
			}
			else
				JOptionPane.showMessageDialog(null, "Error, please select valid option");
		}
	}
}

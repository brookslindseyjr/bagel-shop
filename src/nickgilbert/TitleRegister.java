package nickgilbert;
/**
 * Sets title at top of register
 * @author Nick Gilbert
 */
import javax.swing.*;

import java.awt.*;
import java.awt.Event.*;

public class TitleRegister extends JPanel {
	private JPanel titlePanel;
	private JLabel titleLabel;
	
	public TitleRegister() {
	 	titleLabel = new JLabel("Order Entry Screen", SwingConstants.CENTER);
	 	add(titleLabel);
	}
}

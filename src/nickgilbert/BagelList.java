package nickgilbert;
/**
 * Panel to show bagel options
 * @author Nick Gilbert
 */


import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.sound.sampled.*;

public class BagelList extends JPanel {
	private JPanel questionOne, questionTwo, questionThree;
	public static JRadioButton typeWhite, typeWheat, typeSalt, typeSesame, typePopy;
	public static JRadioButton noSpread, spreadCreamCheese, spreadLowFat, spreadGarlic, spreadButter, spreadPeanutButter, spreadJam;
	public static JCheckBox toppingsLox, toppingsNovaLox, toppingsBanana;
	public ButtonGroup type, spread;
	
	public BagelList() {
		setLayout(new GridLayout(3, 1));
		//Adding Size option
		questionOne = new JPanel();
		questionOne.setLayout(new GridLayout(5, 1));
		questionOne.setBorder(BorderFactory.createTitledBorder("Type"));
		
		//Setting types of bagels to radio buttons
		typeWhite = new JRadioButton("White", true);
		typeWheat = new JRadioButton("Wheat");
		typeSalt = new JRadioButton("Salt");
		typeSesame = new JRadioButton("Sesame");
		typePopy = new JRadioButton("Popy");
		
		type = new ButtonGroup();
		type.add(typeWhite); questionOne.add(typeWhite);
		type.add(typeWheat); questionOne.add(typeWheat);
		type.add(typeSalt); questionOne.add(typeSalt);
		type.add(typeSesame); questionOne.add(typeSesame);
		type.add(typePopy); questionOne.add(typePopy);
		
		add(questionOne);
		
		//Adding Spread Options
		questionTwo = new JPanel();
		questionTwo.setLayout(new GridLayout(7, 1));
		questionTwo.setBorder(BorderFactory.createTitledBorder("Spread"));
		
		noSpread = new JRadioButton("Plain", true);
		spreadCreamCheese = new JRadioButton("Cream Cheese");
		spreadLowFat = new JRadioButton("Lowfat Cream Cheese");
		spreadGarlic = new JRadioButton("Garlic");
		spreadButter = new JRadioButton("Butter");
		spreadPeanutButter = new JRadioButton("Peanut Butter");
		spreadJam = new JRadioButton("Jam");
		
		spread = new ButtonGroup();
		spread.add(noSpread); questionTwo.add(noSpread);
		spread.add(spreadCreamCheese); questionTwo.add(spreadCreamCheese);
		spread.add(spreadLowFat); questionTwo.add(spreadLowFat);
		spread.add(spreadGarlic); questionTwo.add(spreadGarlic);
		spread.add(spreadButter); questionTwo.add(spreadButter);
		spread.add(spreadPeanutButter); questionTwo.add(spreadPeanutButter);
		spread.add(spreadJam); questionTwo.add(spreadJam);
		
		add(questionTwo);
		
		//Adding Topping Options
		questionThree = new JPanel();
		questionThree.setLayout(new GridLayout(3, 1));
		questionThree.setBorder(BorderFactory.createTitledBorder("Toppings"));
		
		toppingsLox = new JCheckBox("Lox");
		toppingsNovaLox = new JCheckBox("Nova Lox");
		toppingsBanana = new JCheckBox("Banana");
		
		questionThree.add(toppingsLox);
		questionThree.add(toppingsNovaLox);
		questionThree.add(toppingsBanana);
		
		add(questionThree);
	}
	
	public static Order buildOrder() { //Method that builds an Order object based on what user chooses
		String orderDescription = "";
		double orderCost = 0.00;
		
		//Type
		if(typeWhite.isSelected()) {
			orderDescription += "White Bagel ";
			orderCost += 2.00;
		}
		else if(typeWheat.isSelected()) {
			orderDescription += "Wheat Bagel ";
			orderCost += 2.00;
		}
		else if(typeSalt.isSelected()) {
			orderDescription += "Salty Bagel ";
			orderCost += 2.50;
		}
		else if(typeSesame.isSelected()) {
			orderDescription += "Sesame Bagel ";
			orderCost += 2.50;
		}
		else if(typePopy.isSelected()) {
			orderDescription += "Popy Bagel ";
			orderCost += 2.50;
		}
		else
			JOptionPane.showMessageDialog(null, "Error, please select valid option");
		
		//Spread
		if(noSpread.isSelected()) {
			orderDescription += "\n    -Plain ";
			orderCost += 0;
		}
		else if(spreadCreamCheese.isSelected()) {
			orderDescription += "\n   -Cream Cheese ";
			orderCost += .25;
		}
		else if(spreadLowFat.isSelected()) {
			orderDescription += "\n   -Lowfat Cream Cheese ";
			orderCost += .25;
		}
		else if(spreadGarlic.isSelected()) {
			orderDescription += "\n   -Garlic Cream Cheese ";
			orderCost += .50;
		}
		else if(spreadButter.isSelected()) {
			orderDescription += "\n   -Butter ";
			orderCost += .50;
		}
		else if(spreadPeanutButter.isSelected()) {
			orderDescription += "\n   -Peanut Butter ";
			orderCost += .50;
		}
		else if(spreadJam.isSelected()) {
			orderDescription += "\n   -Jam";
			orderCost += .50;
		}
		else
			JOptionPane.showMessageDialog(null, "Error, please select valid type");
		
		//Toppings
		if(toppingsLox.isSelected()) {
			orderDescription += "\n   -Lox ";
			orderCost += 2.25;
		}
		if(toppingsNovaLox.isSelected()) {
			orderDescription += "\n   -Nova Lox ";
			orderCost += 2.25;
		}
		if(toppingsBanana.isSelected()) {
			orderDescription += "\n   -Banana ";
			orderCost += 1.00;
			try { //Audio code adapted from http://stackoverflow.com/questions/2416935/how-to-play-wav-files-with-java
			    File yourFile = new File("ohBanana.wav");
			    AudioInputStream stream;
			    AudioFormat format;
			    DataLine.Info info;
			    Clip clip;

			    stream = AudioSystem.getAudioInputStream(yourFile);
			    format = stream.getFormat();
			    info = new DataLine.Info(Clip.class, format);
			    clip = (Clip) AudioSystem.getLine(info);
			    clip.open(stream);
			    clip.start();
			}
			catch (Exception e) {
				System.out.println("Audio file ohBanana.wav not found!");
			}
			
		}
		
		return (new Order(orderDescription, orderCost));
	}
}

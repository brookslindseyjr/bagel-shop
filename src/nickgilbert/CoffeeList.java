package nickgilbert;
/**
 * Class to show coffee options when Coffee option is selected
 * @author Nick Gilbert
 */
import javax.swing.*;

import java.awt.*;
import java.awt.Event.*;

public class CoffeeList extends JPanel {
	private JPanel questionOne, questionTwo, questionThree;
	public static JRadioButton sizeSmall, sizeMedium, sizeLarge, typeRegular, typeDecaf, typeDark;
	public static JCheckBox extrasCream, extrasSugar;
	private ButtonGroup size, type;
	
	public CoffeeList() {
		setLayout(new GridLayout(3, 1));
		//Adding Size option
		questionOne = new JPanel();
		questionOne.setLayout(new GridLayout(3, 1));
		questionOne.setBorder(BorderFactory.createTitledBorder("Size"));
		
		sizeSmall = new JRadioButton("Small", true);
		sizeMedium = new JRadioButton("Medium");
		sizeLarge = new JRadioButton("Large");
		
		size = new ButtonGroup();
		size.add(sizeSmall); questionOne.add(sizeSmall);
		size.add(sizeMedium); questionOne.add(sizeMedium);
		size.add(sizeLarge); questionOne.add(sizeLarge);
		
		add(questionOne);
		
		//Adding Type Options
		questionTwo = new JPanel();
		questionTwo.setLayout(new GridLayout(3, 1));
		questionTwo.setBorder(BorderFactory.createTitledBorder("Type"));
		
		typeRegular = new JRadioButton("Regular", true);
		typeDecaf = new JRadioButton("Decaf");
		typeDark = new JRadioButton("Dark Roast");
		
		type = new ButtonGroup();
		type.add(typeRegular); questionTwo.add(typeRegular);
		type.add(typeDecaf); questionTwo.add(typeDecaf);
		type.add(typeDark); questionTwo.add(typeDark);
		
		add(questionTwo);
		
		//Adding Extras Options
		questionThree = new JPanel();
		questionThree.setLayout(new GridLayout(3, 1));
		questionThree.setBorder(BorderFactory.createTitledBorder("Extras"));
		
		extrasCream = new JCheckBox("Cream");
		extrasSugar = new JCheckBox("Sugar");
		
		questionThree.add(extrasCream);
		questionThree.add(extrasSugar);
		
		add(questionThree);
	}
	
	public static Order buildOrder() { //Method that builds an Order object based on input into cash register
		String orderDescription = "";
		double orderCost = 0.00;
		
		//Size
		if(sizeSmall.isSelected()) {
			orderDescription += "Sm. ";
			orderCost += 1.00;
		}
		else if(sizeMedium.isSelected()) {
			orderDescription += "Med. ";
			orderCost += 1.50;
		}
		else if(sizeLarge.isSelected()) {
			orderDescription += "Lg. ";
			orderCost += 2.00;
		}
		else
			JOptionPane.showMessageDialog(null, "Error, please select valid option");
		
		//Type
		if(typeRegular.isSelected()) {
			orderDescription += "Regular Coffee ";
			orderCost += 1.00;
		}
		else if(typeDecaf.isSelected()) {
			orderDescription += "Decaf Coffee ";
			orderCost += 1.00;
		}
		else if(typeDark.isSelected()) {
			orderDescription += "Dark Roast Coffee ";
			orderCost += 1.25;
		}
		else
			JOptionPane.showMessageDialog(null, "Error, please select valid type");
		
		//Extras
		if(extrasCream.isSelected()) {
			orderDescription += "\n   -Cream ";
			orderCost += .25;
		}
		if(extrasSugar.isSelected()) {
			orderDescription += "\n   -Sugar ";
			orderCost += .25;
		}
		
		return (new Order(orderDescription, orderCost));
	}
}

package nickgilbert;
/**
 * Class to show selected register items person wants to purchase
 * @author Nick Gilbert
 */
import javax.swing.*;
import java.awt.*;
import java.awt.Event.*;
import java.util.*;

public class Receipt extends JPanel {
	public static JTextArea receipt;
	public static ArrayList<Order> orders = new ArrayList<Order>();
	private static JScrollPane scroller;
	
	public Receipt() {
		receipt = new JTextArea(37, 22);
		receipt.setEditable(false); //User can't edit receipt
		scroller = new JScrollPane(receipt);
		add(scroller);
	}
	
	public static void writeToReceipt(Order o) {
		String description = o.getDescription();
		double cost = o.getCost();
		String costToPrint = "\t$" + String.format("%-20.2f", cost);
		receipt.append(description + costToPrint + "\n");
		orders.add(o);
	}
	
	public static void totalReceipt() { //Method to total items on receipt
		double subTotal = 0.00;
		double tax = 0.00;
		double total = 0.00;
		for(int i = 0; i < orders.size(); i++) {
			Order p = orders.get(i);
			subTotal += p.getCost();
		}
		String subTotalToPrint = String.format("%-20.2f", subTotal);
		receipt.append("\n\nTotal\t$" + subTotalToPrint + "\n");
		tax = subTotal * .08;
		String taxToPrint = String.format("%-20.2f", tax);
		receipt.append("Tax\t$" + taxToPrint + "\n");
		total = subTotal + tax;
		String totalToPrint = String.format("%-20.2f", total);
		receipt.append("Total\t$" + totalToPrint + "\n");
	}
	
	public static void clearReceipt() { //Clears receipt
		receipt.setText("");
		orders.clear();
	}
}

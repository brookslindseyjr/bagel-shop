package nickgilbert;
/**
 * Class to represent an order entered into the cash register
 * Makes it easier to write to receipt tape
 * @author Nick Gilbert
 */
public class Order {
	private String description;
	private double cost;
	
	public Order(String d, double c) {
		description = d;
		cost = c;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}
}

package nickgilbert;
/**
 * Class to add buttons to GUI
 * @author Nick Gilbert
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ControlButtons extends JPanel {
	private JButton addItem, total, newOrder;
	private boolean toClear;
	
	public ControlButtons() {
		addItem = new JButton("Add Item");
    	total =  new JButton("Total");
    	newOrder = new JButton("New Order");
    	
    	addItem.addActionListener(new ButtonListener());
    	total.addActionListener(new ButtonListener());
    	newOrder.addActionListener(new ButtonListener());
    	
    	add(addItem); add(total); add(newOrder);
	}
	
	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Order o; //Order object to be built based on selected options
			String actionCommand = e.getActionCommand();
			
			//Builds order using options showing in middle panel
			if(actionCommand.equalsIgnoreCase("Add Item")) {
				if(toClear == true) {
					Receipt.clearReceipt();
					toClear = false;
				}
				if(CoffeeShop.getSelectedList() == 0) {
					o = CoffeeList.buildOrder();
				}
				else if(CoffeeShop.getSelectedList() == 1) {
					o = BagelList.buildOrder();
				}
				else
					o = PastryList.buildOrder();
				Receipt.writeToReceipt(o);
			}
			else if(actionCommand.equalsIgnoreCase("Total")) { //Totals receipt
				toClear = true;
				Receipt.totalReceipt();
			}
			else if(actionCommand.equalsIgnoreCase("New Order")) { //Clears receipt
				Receipt.clearReceipt();
			}
			else {
				JOptionPane.showMessageDialog(null, "Error, please select valid option");
			}
		}
	}
}

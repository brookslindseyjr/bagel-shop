package nickgilbert;
/**
 * Class to give a top menu to the cash register
 * @author Nick Gilbert
 */
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class TopMenu extends JPanel {
	private static JMenuBar menuBar;
	private JMenu fileMenu, optionMenu, helpMenu;
	private JMenuItem fileItem, optionItem, helpItem;
	
	public TopMenu() {
		//Initializing menu
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File (ALT+F)");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		optionMenu = new JMenu("Options (ALT+O)");
		optionMenu.setMnemonic(KeyEvent.VK_O);
		helpMenu = new JMenu("Help (ALT+H)");
		helpMenu.setMnemonic(KeyEvent.VK_H);
		
		//Creating Show Bagel Options
		fileItem = new JMenuItem("Show Bagel Options (ALT+B)");
		fileItem.setMnemonic(KeyEvent.VK_B);
		fileItem.addActionListener(new ChangeOptionListener());
		fileMenu.add(fileItem);
		
		//Creating Show Coffee Options
		fileItem = new JMenuItem("Show Pastry Options (ALT+P)");
		fileItem.setMnemonic(KeyEvent.VK_P);
		fileItem.addActionListener(new ChangeOptionListener());
		fileMenu.add(fileItem);
		
		//Creating Show Coffee Options
		fileItem = new JMenuItem("Show Coffee Options (ALT+C)");
		fileItem.setMnemonic(KeyEvent.VK_C);
		fileItem.addActionListener(new ChangeOptionListener());
		fileMenu.add(fileItem);
		
		fileMenu.addSeparator();
		
		//Creating Total Button
		fileItem = new JMenuItem("Total (ALT+T)");
		fileItem.setMnemonic(KeyEvent.VK_T);
		fileItem.addActionListener(new FileMenuListener());
		fileMenu.add(fileItem);
		
		//Creating New Order Button
		fileItem = new JMenuItem("New Order (ALT+N)");
		fileItem.setMnemonic(KeyEvent.VK_N);
		fileItem.addActionListener(new FileMenuListener());
		fileMenu.add(fileItem);
		
		fileMenu.addSeparator();
		
		//Creating Quit Button
		fileItem = new JMenuItem("Quit (ALT+Q)");
		fileItem.setMnemonic(KeyEvent.VK_Q);
		fileItem.addActionListener(new QuitListener());
		fileMenu.add(fileItem);
		
		//Creating Color Scheme Button
		optionItem = new JMenuItem("Color Scheme (ALT+C)");
		optionItem.setMnemonic(KeyEvent.VK_C);
		optionItem.addActionListener(new OptionMenuListener());
		optionMenu.add(optionItem);
		
		//Creating Instructions Button
		helpItem = new JMenuItem("How to Use (ALT+H)");
		helpItem.setMnemonic(KeyEvent.VK_H);
		helpItem.addActionListener(new HelpMenuListener());
		helpMenu.add(helpItem);
		
		helpMenu.addSeparator();
		
		//Creating About Button
		helpItem = new JMenuItem("About (ALT+A)");
		helpItem.setMnemonic(KeyEvent.VK_A);
		helpItem.addActionListener(new HelpMenuListener());
		helpMenu.add(helpItem);
		
		//Adding components
		menuBar.add(fileMenu);
		menuBar.add(optionMenu);
		menuBar.add(helpMenu);
		add(menuBar);
	}
	
	public static JMenuBar getMenuBar() {
		return menuBar;
	}
	
	private class FileMenuListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equalsIgnoreCase("Total (Alt+T)")) {
				Receipt.totalReceipt();
			}
			if(e.getActionCommand().equalsIgnoreCase("New Order (Alt+N)")) {
				Receipt.clearReceipt();
			}
		}
	}
	
	public class ChangeOptionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equalsIgnoreCase("Show Bagel Options (ALT+B)")) {
				CoffeeShop.changeOptions(1);
			}
			if(e.getActionCommand().equalsIgnoreCase("Show Pastry Options (ALT+P)")) {
				CoffeeShop.changeOptions(2);
			}
			if(e.getActionCommand().equalsIgnoreCase("Show Coffee Options (ALT+C)")) {
				CoffeeShop.changeOptions(0);
			}
		}
	}
	
	private class QuitListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equalsIgnoreCase("Quit (Alt+Q)")); {
				System.out.println(e.getActionCommand().equalsIgnoreCase("Quit (Alt+Q)"));
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog(null,  "Are you sure you'd like to quit?");
				if(dialogResult == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}			
		}
	}
	
	private class OptionMenuListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Color defaultColor = new Color(238, 238, 238);
			if(e.getActionCommand().equalsIgnoreCase("Color Scheme (Alt+C)")) {
				Color newBackground = JColorChooser.showDialog(null, "Change Color Scheme!", defaultColor);
				CoffeeShop.changeColorScheme(newBackground);
			}
		}
	}
	
	private class HelpMenuListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equalsIgnoreCase("How to Use (Alt+H)")) {
				JOptionPane.showMessageDialog(null, "How to Use\n-----------------\n"
						+ "Use the radio buttons in the first column to pick "
						+ "the type of product you want to ring up. Then use the "
						+ "middle panel\nto specify which product of that type you want "
						+ "to enter. From there use the ADD ITEM button to add that item\n"
						+ "to the receipt (shown in the right column). When done, press "
						+ "the TOTAL button to see the customer's total. For a new order, \n"
						+ "press the NEW ORDER button. Further options are availible in "
						+ "the menu system at the top of the screen.");
			}
			if(e.getActionCommand().equalsIgnoreCase("About (Alt+A)")) {
				JOptionPane.showMessageDialog(null, "About CoffeeShop\n--------------------------\n"
						+ "A cash register simulator for a theoretical coffee, bagel and "
						+ "pastry shop!\n\nCreated by Nick Gilbert for Dr. Humphries' 2014 "
						+ "\"COS150: Advanced Programming Methodology\" class at Covenant College");
			}
		}
	}
}
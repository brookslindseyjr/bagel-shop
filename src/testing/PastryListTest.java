package testing;

import static org.junit.Assert.*;
import nickgilbert.Order;
import nickgilbert.PastryList;

import org.junit.Before;
import org.junit.Test;

public class PastryListTest {
	
	PastryList pastryList;

	@Before
	public void setUp() throws Exception {
		pastryList = new PastryList();
	}
	
	@Test
	public void testBuildOrder01() {
		pastryList.pastryOptions.setSelectedIndex(0);
		
		Order order = pastryList.buildOrder();
		double expected = 1.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}

	@Test
	public void testBuildOrder02() {
		pastryList.pastryOptions.setSelectedIndex(1);
		
		Order order = pastryList.buildOrder();
		double expected = 1.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder03() {
		pastryList.pastryOptions.setSelectedIndex(2);
		
		Order order = pastryList.buildOrder();
		double expected = 1.5;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder04() {
		pastryList.pastryOptions.setSelectedIndex(3);
		
		Order order = pastryList.buildOrder();
		double expected = 2.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
}

package testing;
import static org.junit.Assert.*;
import nickgilbert.BagelList;
import nickgilbert.Order;

import org.junit.Before;
import org.junit.Test;


public class BagelListTest {
	
	private BagelList bagelList;

	@Before
	public void setUp() throws Exception {
		bagelList = new BagelList();
	}
	
	@Test
	public void testBuildOrder01() {
		bagelList.typeWhite.setSelected(true);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(true);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 7.5;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder02() {
		bagelList.typeWhite.setSelected(true);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(true);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 2.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder03() {
		bagelList.typeWhite.setSelected(true);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(true);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 4.5;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder04() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(true);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(true);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 5.5;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder05() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(true);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(true);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 7.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder06() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(true);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(true);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 3.75;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder07() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(true);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(true);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 4.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder08() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(true);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(true);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 7.5;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder09() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(true);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(true);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 5.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder10() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(true);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(true);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 6.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder11() {
		bagelList.typeWhite.setSelected(true);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(true);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 5.75;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder12() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(true);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(true);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 4.75;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder13() {
		bagelList.typeWhite.setSelected(true);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(true);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 2.5;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder14() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(true);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(true);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 8.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder15() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(true);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(true);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 4.75;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder16() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(true);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(true);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 5.75;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder17() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(true);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(true);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 6.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder18() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(true);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(true);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 5.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder19() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(true);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(true);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 8.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder20() {
		bagelList.typeWhite.setSelected(true);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(true);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 2.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder21() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(true);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(true);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 3.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder22() {
		bagelList.typeWhite.setSelected(true);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(true);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 8.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder23() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(true);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(true);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 5.75;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder24() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(true);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(true);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 5.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder25() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(true);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(true);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 6.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder26() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(true);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(true);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 5.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder27() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(true);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(true);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 6.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder28() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(true);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(true);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 5.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder29() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(true);
		
		bagelList.noSpread.setSelected(true);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 5.75;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder30() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(true);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(true);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 7.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder31() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(true);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(true);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 4.5;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder32() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(true);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(true);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 3.5;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder33() {
		bagelList.typeWhite.setSelected(true);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(true);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(false);

		Order order = bagelList.buildOrder();
		double expected = 7.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder34() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(true);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(true);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 4.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder35() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(true);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(true);
		bagelList.spreadJam.setSelected(false);
		
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 4.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder36() {
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(true);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);
		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(true);
		
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(true);

		Order order = bagelList.buildOrder();
		double expected = 8.5;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
}

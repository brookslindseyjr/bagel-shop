package testing;

import static org.junit.Assert.*;
import nickgilbert.Order;

import org.junit.Test;

public class OrderTest {
	
	Order order;

	@Test
	public void testGetDescription() {
		order = new Order("Test Description", 3.5);
		
		String expected = "Test Description";
		String actual = order.getDescription();
		
		assertEquals(expected, actual);
	}

	@Test
	public void testSetDescription() {
		order = new Order("Test Description", 3.5);
		
		order.setDescription("New Desc");
		
		String expected = "New Desc";
		String actual = order.getDescription();
		
		assertEquals(expected, actual);
	}

	@Test
	public void testGetCost() {
		order = new Order("Test Description", 3.5);
		
		double expected = 3.5;
		double actual = order.getCost();
		double delta = 0.0;
		
		assertEquals(expected, actual, delta);
	}

	@Test
	public void testSetCost() {
		order = new Order("Test Description", 3.5);
		
		order.setCost(2.4);
		
		double expected = 2.4;
		double actual = order.getCost();
		double delta = 0.0;
		
		assertEquals(expected, actual, delta);
	}

}

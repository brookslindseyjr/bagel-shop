package testing;

import static org.junit.Assert.*;
import nickgilbert.CoffeeList;
import nickgilbert.Order;

import org.junit.Before;
import org.junit.Test;


public class CoffeeListTest {
	
	private CoffeeList coffeeList;

	@Before
	public void setUp() throws Exception {
		coffeeList = new CoffeeList();
	}
	
	@Test
	public void testBuildOrder01() {
		coffeeList.sizeSmall.setSelected(true);
		coffeeList.sizeMedium.setSelected(false);
		coffeeList.sizeLarge.setSelected(false);
		
		coffeeList.typeRegular.setSelected(true);
		coffeeList.typeDecaf.setSelected(false);
		coffeeList.typeDark.setSelected(false);
		
		coffeeList.extrasCream.setSelected(true);
		coffeeList.extrasSugar.setSelected(true);
		
		Order order = coffeeList.buildOrder();
		double expected = 2.5;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder02() {
		coffeeList.sizeSmall.setSelected(true);
		coffeeList.sizeMedium.setSelected(false);
		coffeeList.sizeLarge.setSelected(false);
		
		coffeeList.typeRegular.setSelected(false);
		coffeeList.typeDecaf.setSelected(true);
		coffeeList.typeDark.setSelected(false);
		
		coffeeList.extrasCream.setSelected(false);
		coffeeList.extrasSugar.setSelected(false);
		
		Order order = coffeeList.buildOrder();
		double expected = 2.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder03() {
		coffeeList.sizeSmall.setSelected(false);
		coffeeList.sizeMedium.setSelected(true);
		coffeeList.sizeLarge.setSelected(false);
		
		coffeeList.typeRegular.setSelected(true);
		coffeeList.typeDecaf.setSelected(false);
		coffeeList.typeDark.setSelected(false);
		
		coffeeList.extrasCream.setSelected(false);
		coffeeList.extrasSugar.setSelected(true);
		
		Order order = coffeeList.buildOrder();
		double expected = 2.75;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder04() {
		coffeeList.sizeSmall.setSelected(false);
		coffeeList.sizeMedium.setSelected(true);
		coffeeList.sizeLarge.setSelected(false);
		
		coffeeList.typeRegular.setSelected(false);
		coffeeList.typeDecaf.setSelected(true);
		coffeeList.typeDark.setSelected(false);
		
		coffeeList.extrasCream.setSelected(true);
		coffeeList.extrasSugar.setSelected(false);
		
		Order order = coffeeList.buildOrder();
		double expected = 2.75;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder05() {
		coffeeList.sizeSmall.setSelected(false);
		coffeeList.sizeMedium.setSelected(false);
		coffeeList.sizeLarge.setSelected(true);
		
		coffeeList.typeRegular.setSelected(false);
		coffeeList.typeDecaf.setSelected(false);
		coffeeList.typeDark.setSelected(true);
		
		coffeeList.extrasCream.setSelected(true);
		coffeeList.extrasSugar.setSelected(true);
		
		Order order = coffeeList.buildOrder();
		double expected = 3.75;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder06() {
		coffeeList.sizeSmall.setSelected(false);
		coffeeList.sizeMedium.setSelected(false);
		coffeeList.sizeLarge.setSelected(true);
		
		coffeeList.typeRegular.setSelected(true);
		coffeeList.typeDecaf.setSelected(false);
		coffeeList.typeDark.setSelected(false);
		
		coffeeList.extrasCream.setSelected(false);
		coffeeList.extrasSugar.setSelected(false);
		
		Order order = coffeeList.buildOrder();
		double expected = 3.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder07() {
		coffeeList.sizeSmall.setSelected(true);
		coffeeList.sizeMedium.setSelected(false);
		coffeeList.sizeLarge.setSelected(false);
		
		coffeeList.typeRegular.setSelected(false);
		coffeeList.typeDecaf.setSelected(false);
		coffeeList.typeDark.setSelected(true);
		
		coffeeList.extrasCream.setSelected(false);
		coffeeList.extrasSugar.setSelected(false);
		
		Order order = coffeeList.buildOrder();
		double expected = 2.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder08() {
		coffeeList.sizeSmall.setSelected(false);
		coffeeList.sizeMedium.setSelected(true);
		coffeeList.sizeLarge.setSelected(false);
		
		coffeeList.typeRegular.setSelected(false);
		coffeeList.typeDecaf.setSelected(true);
		coffeeList.typeDark.setSelected(false);
		
		coffeeList.extrasCream.setSelected(true);
		coffeeList.extrasSugar.setSelected(true);
		
		Order order = coffeeList.buildOrder();
		double expected = 3.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder09() {
		coffeeList.sizeSmall.setSelected(false);
		coffeeList.sizeMedium.setSelected(true);
		coffeeList.sizeLarge.setSelected(false);
		
		coffeeList.typeRegular.setSelected(false);
		coffeeList.typeDecaf.setSelected(false);
		coffeeList.typeDark.setSelected(true);
		
		coffeeList.extrasCream.setSelected(false);
		coffeeList.extrasSugar.setSelected(true);
		
		Order order = coffeeList.buildOrder();
		double expected = 3.0;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testBuildOrder10() {
		coffeeList.sizeSmall.setSelected(false);
		coffeeList.sizeMedium.setSelected(false);
		coffeeList.sizeLarge.setSelected(true);
		
		coffeeList.typeRegular.setSelected(false);
		coffeeList.typeDecaf.setSelected(true);
		coffeeList.typeDark.setSelected(false);
		
		coffeeList.extrasCream.setSelected(true);
		coffeeList.extrasSugar.setSelected(false);
		
		Order order = coffeeList.buildOrder();
		double expected = 3.25;
		double actual = order.getCost();
		double delta = 0.0;
		assertEquals(expected, actual, delta);
	}
}

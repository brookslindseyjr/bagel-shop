package testing;

import static org.junit.Assert.*;

import java.util.ArrayList;

import nickgilbert.BagelList;
import nickgilbert.CoffeeList;
import nickgilbert.Order;
import nickgilbert.PastryList;
import nickgilbert.Receipt;

import org.junit.Before;
import org.junit.Test;

public class ReceiptTest {

	private Receipt receipt;
	private CoffeeList coffeeList;
	private PastryList pastryList;
	private BagelList bagelList;
	
	@Before
	public void setUp() throws Exception {
		receipt = new Receipt();
		receipt.orders = new ArrayList<Order>();
		coffeeList = new CoffeeList();
		pastryList = new PastryList();
		bagelList = new BagelList();
	}
	
	@Test
	public void testWriteToReceipt01() {
		receipt.receipt.setText("");
		receipt.orders.clear();
		
		Order order1 = new Order("Test Description", 5.00);
		receipt.writeToReceipt(order1);
		
		String expected = "Test Description\t$5.00                \n";
		String actual = receipt.receipt.getText();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWriteToReceipt02() {
		receipt.receipt.setText("");
		receipt.orders.clear();
		
		Order order1 = new Order("Product 1 Order", 6.75);
		Order order2 = new Order("Product 2 Order", 7.35);
		receipt.writeToReceipt(order1);
		receipt.writeToReceipt(order2);
		
		String expected = "Product 1 Order\t$6.75                \nProduct 2 Order\t$7.35                \n";
		String actual = receipt.receipt.getText();
		assertEquals(expected, actual);
	}

	@Test
	public void testTotalReceipt() {
		receipt.receipt.setText("");
		receipt.orders.clear();
		
		Order order1 = new Order("Product 1 Order", 6.75);
		Order order2 = new Order("Product 2 Order", 7.35);
		receipt.writeToReceipt(order1);
		receipt.writeToReceipt(order2);
		receipt.totalReceipt();
		
		String expected = "Product 1 Order\t$6.75                \nProduct 2 Order\t$7.35                "
				+ "\n\n\nTotal\t$14.10               \nTax\t$1.13                \nTotal\t$15.23               \n";
		String actual = receipt.receipt.getText();
		assertEquals(expected, actual);
	}

	@Test
	public void testClearReceipt() {
		receipt.receipt.setText("");
		receipt.orders.clear();
		
		Order order1 = new Order("Product 1 Order", 6.75);
		Order order2 = new Order("Product 2 Order", 7.35);
		receipt.writeToReceipt(order1);
		receipt.writeToReceipt(order2);
		receipt.totalReceipt();
		receipt.clearReceipt();
		
		String expected = "";
		String actual = receipt.receipt.getText();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testMultipleProducts01(){
		receipt.receipt.setText("");
		receipt.orders.clear();
		
		// Setup Coffee
		coffeeList.sizeSmall.setSelected(true);
		coffeeList.sizeMedium.setSelected(false);
		coffeeList.sizeLarge.setSelected(false);
		coffeeList.typeRegular.setSelected(true);
		coffeeList.typeDecaf.setSelected(false);
		coffeeList.typeDark.setSelected(false);
		coffeeList.extrasCream.setSelected(true);
		coffeeList.extrasSugar.setSelected(true);
		
		// Setup Pastry
		pastryList.pastryOptions.setSelectedIndex(1);
		
		// Setup Bagel
		bagelList.typeWhite.setSelected(true);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);		
		bagelList.noSpread.setSelected(true);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);	
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(true);
		bagelList.toppingsBanana.setSelected(true);
		
		receipt.writeToReceipt(coffeeList.buildOrder());
		receipt.writeToReceipt(pastryList.buildOrder());
		receipt.writeToReceipt(bagelList.buildOrder());
		receipt.totalReceipt();
		
		String expected = "Sm. Regular Coffee \n   -Cream \n   -Sugar \t$2.50                \nPrune Danish\t$1.25"
				+ "                \nWhite Bagel \n    -Plain \n   -Lox \n   -Nova Lox \n   "
				+ "-Banana \t$7.50                \n\n\nTotal\t$11.25               \nTax\t$0.90                \nTotal\t$12.15               \n";
		String actual = receipt.receipt.getText();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testMultipleProducts02(){
		receipt.receipt.setText("");
		receipt.orders.clear();
		
		// Setup Coffee
		coffeeList.sizeSmall.setSelected(false);
		coffeeList.sizeMedium.setSelected(true);
		coffeeList.sizeLarge.setSelected(false);
		coffeeList.typeRegular.setSelected(true);
		coffeeList.typeDecaf.setSelected(false);
		coffeeList.typeDark.setSelected(false);
		coffeeList.extrasCream.setSelected(true);
		coffeeList.extrasSugar.setSelected(false);
		
		// Setup Pastry
		pastryList.pastryOptions.setSelectedIndex(0);
		
		// Setup Bagel
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(true);
		bagelList.typeSesame.setSelected(false);
		bagelList.typePopy.setSelected(false);		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(false);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(true);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);	
		bagelList.toppingsLox.setSelected(true);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(true);
		
		receipt.writeToReceipt(coffeeList.buildOrder());
		receipt.writeToReceipt(pastryList.buildOrder());
		receipt.writeToReceipt(bagelList.buildOrder());
		receipt.totalReceipt();
		
		String expected = "Med. Regular Coffee \n   -Cream \t$2.75                "
				+ "\nApricot Danish\t$1.25                \nSalty Bagel \n   "
				+ "-Garlic Cream Cheese \n   -Lox \n   -Banana \t$6.25                "
				+ "\n\n\nTotal\t$10.25               \nTax\t$0.82                "
				+ "\nTotal\t$11.07               \n";
		String actual = receipt.receipt.getText();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testMultipleProducts03(){
		receipt.receipt.setText("");
		receipt.orders.clear();
		
		// Setup Coffee
		coffeeList.sizeSmall.setSelected(false);
		coffeeList.sizeMedium.setSelected(false);
		coffeeList.sizeLarge.setSelected(true);
		coffeeList.typeRegular.setSelected(false);
		coffeeList.typeDecaf.setSelected(false);
		coffeeList.typeDark.setSelected(true);
		coffeeList.extrasCream.setSelected(false);
		coffeeList.extrasSugar.setSelected(false);
		
		// Setup Pastry
		pastryList.pastryOptions.setSelectedIndex(2);
		
		// Setup Bagel
		bagelList.typeWhite.setSelected(false);
		bagelList.typeWheat.setSelected(false);
		bagelList.typeSalt.setSelected(false);
		bagelList.typeSesame.setSelected(true);
		bagelList.typePopy.setSelected(false);		
		bagelList.noSpread.setSelected(false);
		bagelList.spreadCreamCheese.setSelected(true);
		bagelList.spreadLowFat.setSelected(false);
		bagelList.spreadGarlic.setSelected(false);
		bagelList.spreadButter.setSelected(false);
		bagelList.spreadPeanutButter.setSelected(false);
		bagelList.spreadJam.setSelected(false);	
		bagelList.toppingsLox.setSelected(false);
		bagelList.toppingsNovaLox.setSelected(false);
		bagelList.toppingsBanana.setSelected(true);
		
		receipt.writeToReceipt(coffeeList.buildOrder());
		receipt.writeToReceipt(pastryList.buildOrder());
		receipt.writeToReceipt(bagelList.buildOrder());
		receipt.totalReceipt();
		
		String expected = "Lg. Dark Roast Coffee \t$3.25                "
				+ "\nCrossant\t$1.50                \nSesame Bagel \n   "
				+ "-Cream Cheese \n   -Banana \t$3.75                "
				+ "\n\n\nTotal\t$8.50                \nTax\t$0.68                "
				+ "\nTotal\t$9.18                \n";
		String actual = receipt.receipt.getText();
		assertEquals(expected, actual);
	}

}

package testing;

import static org.junit.Assert.*;

import nickgilbert.CoffeeShop;

import org.junit.Before;
import org.junit.Test;

public class CoffeeShopTest {
	
	CoffeeShop coffeeShop;
	int selectedList;

	@Before
	public void setUp() throws Exception {
		coffeeShop = new CoffeeShop();
		selectedList = 0;
	}

	@Test
	public void testGetSetSelectedList01() {
		coffeeShop.setSelectedList(1);
		
		int expected = 1;
		int actual = coffeeShop.getSelectedList();
		int delta = 0;
		
		assertEquals(expected, actual, delta);
	}
	
	@Test
	public void testGetSetSelectedList02() {
		coffeeShop.setSelectedList(2);
		
		int expected = 2;
		int actual = coffeeShop.getSelectedList();
		int delta = 0;
		
		assertEquals(expected, actual, delta);
	}

}
